/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.crud.Sjava.controlador;

/**
 *
 * @author driss
 */


import com.crud.Sjava.modelo.Producto;
import com.crud.Sjava.modelo.Productoo;
import com.crud.Sjava.servicio.Rrepository;
import jakarta.validation.Valid;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

//declaramos @controller en donde se manejaran las solicitud HTTP con @RequestMapping que seran manejadas en la direccion vistaproductos que son httml dinamicos
@Controller
@RequestMapping("/vistaproductos")



public class Productocontrolador {
    
    
// esto no realizara las inyecciones de dependencias de manera automatica
    @Autowired
    
    // como realizamos un JPARepository para los metodos crud aca estamos indicando que Rrepository sera igual a repo que mas adelante sera usado para los metodos crud
    private Rrepository repo;
    
  

// metodo para listar los productos
    @GetMapping({"", "/"}) // manejara solicitudes GET y las rutas que seran las que envian la solicitud en este caso vistaproductos/lista
    public String mostrarproductolista(Model model) {  // indicamos que se pasaran datos del controlador a la vista con el parametro Model
        List<Producto> productos = repo.findAll(Sort.by(Sort.Direction.DESC, "id")); // como estamos pasando una lista indicamos que el orden que se muestren sera en orden desentente
        model.addAttribute("productos", productos); // aca agregaremos los atributos al modelo con   model.addAttribute que vienen del modelo producto
        return "vistaproductos/lista";  // nos mostrara la vista en la cual estara la lista 
    }
    
// metodo para crear los productos
    @GetMapping({"/crear"}) // manejara solicitudes GET y las ruta que seran las que envian la solicitud es la vista vistaproductos/crear
    public String crearvalidacion(Model model) { // indicamos que se pasaran datos del controlador a la vista con el parametro Model para la validacion de los campos
        Productoo productoo = new Productoo(); // creamos uns instancia producto que sera produccto en donde tenemos las validaciones para el formulario
        model.addAttribute("productoo", productoo); // aca agregaremos los atributos al modelo con   model.addAttribute que vienen de producctoo, nuestra valdiacion
        return "vistaproductos/crear";  // nos mostrara la vista en la cual estara la vista crear 
    }

    @PostMapping({"/crear"})  // manejara solicitudes Post que sera la de nuestro formulario alojado en la vista crear
    public String crearproducto(  
            @Valid @ModelAttribute Productoo productoo, // esto aplicara las acciones que tenemos en nuestro productoo de los campos 
            BindingResult result  // esto nos ayudara almacenar los errorres de la validacion
    ) {
        
        // realizamos este if en donde le indicamos que si el campo iamgen esta vacio nos muestre este mensaje 
        if (productoo.getImagen().isEmpty()) {
            result.addError(new FieldError("productoo", "imagen", "La imagen es Obligatoria"));
        }
         // este if indica que si hay errores se devuelva a vistaproductos/crear
        if (result.hasErrors()) {
            return "vistaproductos/crear";
        }

        
       //--------------------------------------------------------------------------------- aca vamos a realizar todo el proceso para guardar la imagen 
        MultipartFile imagen = productoo.getImagen();
        Date creado = new Date();
        String storageFileName = creado.getTime() + "_" + imagen.getOriginalFilename();

        try {
            String cargardirectorio = "public/imagenes"; // direcion en donde se alojaran las imagenes 
            Path cargarpath = Paths.get(cargardirectorio);

            if (!Files.exists(cargarpath)) {
                Files.createDirectories(cargarpath);
            }

            try (InputStream inputStream = imagen.getInputStream()) {
                Path destinoPath = cargarpath.resolve(storageFileName);
                Files.copy(inputStream, destinoPath, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (Exception e) {
            System.out.println("Exception:" + e.getMessage());
        }
        //----------------------------------------------------------------------------------------------

        
        //------------------------------aca estamos capturando los datos del formulario  
        Producto producto = new Producto();
        producto.setNombre(productoo.getNombre());
        producto.setDescripcion(productoo.getDescripcion());
        producto.setPrecio(productoo.getPrecio());
        producto.setImagen(storageFileName);

        repo.save(producto); // este atributo Save es del JPA es en donde va realizar la insercion de nuestro campos de la entidad producto

        return "redirect:/vistaproductos"; // una vez realizada la insercion se deolvera a la lista productos 
    }

    
    
// metodo para mostrar los productos por ID 
    @GetMapping({"/editar"}) // manejara solicitudes Get que sera la de nuestro formulario alojado en la vista editar
    public String mostrarproducto(
            Model model, // indicamos que se pasaran datos del controlador a la vista con el parametro Model 
            @RequestParam int id // pasaremos un id para nuestra lista
    ) {
        try {
            Producto producto = repo.findById(id).get(); // le indicamos que busque el producto por su id, este atributo findById es del JPA para mostrar los datos
            model.addAttribute("producto", producto); // pasaremos el producto encontrado al modelo para que se pueda mostrar en nuestra vista

            Productoo productoo = new Productoo(); // creamos un objeto productoo y le pasaremos sus atributos que se encontraron
            productoo.setNombre(producto.getNombre());
            productoo.setDescripcion(producto.getDescripcion());
            productoo.setPrecio(producto.getPrecio());

            model.addAttribute("productoo", productoo); // pasaremos el producto encontrado al modelo para que se pueda mostrar en nuestra vista
        } catch (Exception e) {  // le indicamos un cath para el manejo de excepciones si da error al encontrar el producto
            System.out.println("Exception:" + e.getMessage());
        }

        return "vistaproductos/editar"; // nos devolvera en donde se mostrara el formnulario con los datos obenidos en este caso es vistaproductos/editar
    }

    @PostMapping({"/editar"})  // manejara solicitudes Post que sera la de nuestro formulario alojado en la vista editar
    public String editarproducto(
            Model model,
            @RequestParam int id,
            @Valid @ModelAttribute Productoo productoo,
            BindingResult result) {

        try {
            Producto producto = repo.findById(id).get();  // le indicamos que busque el producto por su id, este atributo findById es del JPA para mostrar los datos
            model.addAttribute("producto", producto);  // indicamos que se pasaran datos del controlador a la vista con el parametro Model 
            if (result.hasErrors()) { // Si encuentra un error se devolvera a la vista editar 
                return "vistaproductos/editar";

            }
            //_________________________________________ aca borraremos la imagen guardada para reemplazarla
            if (!productoo.getImagen().isEmpty()) {
                
                String cargardirectorio = "public/imagenes/";
                Path imagenv = Paths.get(cargardirectorio + producto.getImagen());

                try {
                    Files.delete(imagenv);
                } catch (Exception e) {
                    System.out.println("Exception:" + e.getMessage());
                }

                MultipartFile imagen = productoo.getImagen();
                Date creado = new Date();
                String storageFileName = creado.getTime() + "_" + imagen.getOriginalFilename();

                try (InputStream inputStream = imagen.getInputStream()) {
                    Files.copy(inputStream, Paths.get(cargardirectorio + storageFileName), StandardCopyOption.REPLACE_EXISTING);
                }
                producto.setImagen(storageFileName);
            }
            //__________________________________________________________________________________
            
            // le pasamos el  productoo y le pasaremos sus atributos que se encontraron
            producto.setNombre(productoo.getNombre());
            producto.setDescripcion(productoo.getDescripcion());
            producto.setPrecio(productoo.getPrecio());

            repo.save(producto); // Con save producto realizaremos la insersion 

            // manejamos  el catch para las exepciones que se lleguen a presentar con 
        } catch (Exception e) {
            System.out.println("Exception:" + e.getMessage());

        }

                return "redirect:/vistaproductos"; // se deolvera a la vista en donde se  encuentra la lista 
    }

    
    // metodo eliminar producto
    @GetMapping({"/eliminar"}) // // manejara solicitudes Get 
    public String eliminarproducto(
            @RequestParam int id // capturaremos el Id 
    ) {
        try {
            Producto producto = repo.findById(id).get(); // le indicamos que busque el producto por su id, este atributo findById es del JPA para mostrar los datos
            
          String cargardirectorio = "public/imagenes/"; // Eliminar la imagen asociada al producto si en dado caso existe 
                Path imagenv = Paths.get(cargardirectorio + producto.getImagen());
                
                try{
                    Files.delete(imagenv);
                }catch (Exception e) {
            System.out.println("Exception:" + e.getMessage());
                }
                
                
                repo.delete(producto); // este atributo delte  es del JPA es en donde va realizar la eliminacion de nuestro campos de la entidad producto
        } catch (Exception e) {
            System.out.println("Exception:" + e.getMessage());
        }
        
        
                return "redirect:/vistaproductos"; // devolvera a la lista productos
    }
}
