package com.crud.Sjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SjavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SjavaApplication.class, args);
	}

}
