/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.crud.Sjava.servicio;

// importamos la entidad producto la cual vamos a trabajar 
import com.crud.Sjava.modelo.Producto; 

// para hacer uso de JPA importamos JpaRepository para los metodos del crud
import org.springframework.data.jpa.repository.JpaRepository;

/*
Este repository nos va a permitir los metodos del crud con JpaRepository de JPA esta funcion nos permite obtimizar codigo ya que las funciones
que integra por defecto  Spring Boo
*/

/**
 *
 * @author driss
 */
// heredamos JpaRepository   y le pasamos la entidad producto con su llave primaria "integer" lo que hace refencia al Iid
public interface Rrepository extends JpaRepository<Producto, Integer> {
    
  
    
}
