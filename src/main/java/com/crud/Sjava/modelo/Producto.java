/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.crud.Sjava.modelo;

/**
 *
 * @author driss
 */

// ---------------------------------------------- aca definimos la entidad producto en donde haccemos uso de Entity y Table para hacer referencia a la tabla 
import jakarta.persistence.*;

@Entity
@Table(name = "productos")
public class Producto {
    
    // creamos una lalve primaria llamada Id con la instancia GeneratedValue en donde sera autoincrementada usando estos parametros strategy=GenerationType.IDENTITY
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
            
            private int id;
            private String nombre;
            private String descripcion;
            private String precio;
            private String imagen;
//-------------------------------------------------------------------------------------------------------------------------------------------
            
            // constructor sin parametros en donde JPA pueda crear instancias de la entidad
            public int getId() {
        return id;
    }

            
           // Getters y Setters 
    public void setId(int id) {
        this.id= id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
            
    
}
