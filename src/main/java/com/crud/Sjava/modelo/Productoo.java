/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.crud.Sjava.modelo;

/**
 *
 * @author driss
 */


// como vamos hacer uso de un formulario en donde vamos a enviar y recibir datos  se creo la clase productoo en donde se trabajara todo el tema de valdiacion de campos



// vamos a importar MultipartFile para validacion en el input file
import org.springframework.web.multipart.MultipartFile;

// vamos a importar constraints.*para validacion en los de mas input 
import jakarta.validation.constraints.*;


public class Productoo {

    // declaramos NotEmpty y colocamos el nombre de nuestro campo en donde le indicamos que no sera varia y arrojara un mesaje indicando que el campo es olibatorio
    @NotEmpty(message = "Este campo es obligatorio")
    private String nombre;

 // declaramos Size Min y Max para la cantidad de caracteres permitidos en el campo desripcion que sera un textarea
@Size(min = 5, message = "Minimo 5 caracteres")
@Size(max = 300, message = "Maximo 300 caracteres")
private String descripcion;



    @NotEmpty(message = "Ingrese un valor")
    private String precio;

private MultipartFile imagen;


 // Getters y Setters para la manipulacion de los campos 
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public MultipartFile getImagen() {
        return imagen;
    }

    public void setImagen(MultipartFile imagen) {
        this.imagen = imagen;
    }



}
